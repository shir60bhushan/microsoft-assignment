package com.bhushans60.assignmentmsoft;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by bhushan on 7/1/16.
 */
public class CustomEmailListViewHolder extends RecyclerView.ViewHolder{


    public ImageView imageViewCheck;
    public TextView textViewEmail;


    public CustomEmailListViewHolder(View view) {
        super(view);

        this.textViewEmail = (TextView) view.findViewById(R.id.tv_titleHRW_active);

        this.imageViewCheck = (ImageView) view.findViewById(R.id.imv_userHRW_active);

    }
}
