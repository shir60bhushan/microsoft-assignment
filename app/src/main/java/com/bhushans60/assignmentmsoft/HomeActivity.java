package com.bhushans60.assignmentmsoft;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HomeActivity extends AppCompatActivity {


    AdapterSearchListRecycler adapter;
    private RecyclerView mRecyclerViewList;
    SearchView searchAct;
    List<FeedItem> listFeedItems= new ArrayList<FeedItem>();
    String changedText="";
    ProgressDialog working_dialog;
    TextView tv_empty;


    String baseUrl = "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=thumbnail&pithumbsize=200&pilimit=50&generator=prefixsearch&gpssearch=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchAct = (SearchView) findViewById(R.id.search);
tv_empty=(TextView)findViewById(R.id.empty_view);



        SearchView.OnQueryTextListener searchListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Log.d("tttt", "testSubmite");
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if(newText.length() >= 3)
                {
                    Log.d("tttt", "queryTextChangedActivty");

                    changedText=null;
                    changedText=newText;
                    listFeedItems.clear();
                    new SerachRequestTask().execute();
                    return true;

                }


                return false;

            }
        };

        searchAct.setOnQueryTextListener(searchListener);


        mRecyclerViewList = (RecyclerView) findViewById(R.id.recycle_view);
        mRecyclerViewList.setVisibility(View.GONE);

        tv_empty.setVisibility(View.VISIBLE);

        mRecyclerViewList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

    }

    public class SerachRequestTask extends AsyncTask<String, Integer, Double> {


        protected void onPreExecute() {
            super.onPreExecute();
              //working_dialog = ProgressDialog.show(HomeActivity.this, "", "Please wait...", true);

        }

        @Override
        protected Double doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {

                String url = baseUrl + changedText;

                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                //add reuqest header
                con.setRequestMethod("POST");
                con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");


                // Send post request
                con.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
                wr.flush();
                wr.close();


                System.out.println("\nSending 'POST' request to URL : " + url);


                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                //print result

                String reSponse = response.toString();

             //   System.out.println(response.toString());


                JSONObject objMain = new JSONObject(reSponse);

                JSONObject objQuery = objMain.getJSONObject("query");
                JSONObject jobjPages = objQuery.getJSONObject("pages");





                Iterator<?> keys = jobjPages.keys();

                while( keys.hasNext() ) {
                    String key = (String)keys.next();
                    if ( jobjPages.get(key) instanceof JSONObject ) {

                    JSONObject tmpJson= (JSONObject) jobjPages.get(key);
                       // Log.d("tt", "ObjContents " + tmpJson.toString());
                        FeedItem feedItem= new FeedItem();
                      //  Log.d("tt", "ObjContents " + tmpJson.getString("title"));

                        feedItem.setTitle( tmpJson.getString("title"));

                        JSONObject tmpObjThumb=tmpJson.getJSONObject("thumbnail");

                        String urlThumbnail =tmpObjThumb.getString("source");


                       // Log.d("tt", "ObjContents " + urlThumbnail);

                        feedItem.setUrl(urlThumbnail);

                        listFeedItems.add(feedItem);

                    }
                }




            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Double result) {

            if (working_dialog != null) {
                working_dialog.dismiss();
                working_dialog = null;
            }

            mRecyclerViewList.setVisibility(View.VISIBLE);
            tv_empty.setVisibility(View.GONE);

            adapter = new AdapterSearchListRecycler(HomeActivity.this, listFeedItems);
            mRecyclerViewList.setAdapter(adapter);


        }
    }
}
